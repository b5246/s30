const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.6vp9f.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	// To avoid connection error
	useNewUrlParser:true,
	useUnifiedTopology: true
});

app.use(express.json());
app.use(express.urlencoded({extended:true}));


//set notif. for connection success or failure
//connection to the database
let db = mongoose.connection;

// if connection error == output console message
db.on("error",console.error.bind(console,"connection error"));
// if success connection
db.once("open",()=>console.log("We're connected to the cloud database"));

// Create a task schema (BLUEPRINT)
const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		//Default val. are the predefined values for a field
		default:"pending"
	}
});

//Create Models
//Server > Schema > Database > Collection (Mongo DB)
//1st para, collection to store
//2nd para , structure
const Task = mongoose.model("Task", taskSchema);


app.listen(port,()=>console.log(`Server running at port ${port}`));



app.get('/',(req,res)=>{
	res.json({
		name:'Eat',
		status: 200
	})
})


// Create a POST route to CHECK task/CREATE a new task
app.post("/tasks",(req,res)=>{
	Task.findOne({name: req.body.name},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send(`Duplicate task found`)
		} else {
			// Create new task and save 
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				} else{
					return res.status(201).send("New Task created")
				}
			})
		}
	})
})


// GET retrive all the task
app.get('/tasks',(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})


//******************************ACTIVITY HERE

const userSchema = new mongoose.Schema({
	username:String,
	password:String
});

const User = mongoose.model("User", userSchema);


app.post("/signup",(req,res)=>{
	User.findOne({username: req.body.username},(err,result)=>{
		if(result != null && result.username == req.body.username){
			return res.send(`Duplicate task found`)
		} else {
			// Create new task and save 
			let myTask = new User({
				username: req.body.username,
				password: req.body.password
			})
			myTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				} else{
					return res.status(201).send("New Task created")
				}
			})
		}
	})
})

//WIthout null / validation
/*
app.post("/signup",(req,res)=>{
	let myTask = new User({
		username: req.body.username,
		password: req.body.password
	})
	myTask.save((saveErr,savedTask)=>{
		if(saveErr){
			return console.error(saveErr);
		} else{
			return res.status(201).send("New Task created")
		}
	})
})*/

app.get('/signup',(req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

